# climusic
climusic is a simple Python script designed to work seamlessly on both Termux (Android) and traditional Linux terminals. It allows you to play music from YouTube using the command line. 


## Prerequisites

Before using `climusic`, make sure you have the required libraries and VLC installed. You can install them using the following commands:

```bash
pip install ytmusicapi pytube python-vlc
```

## Usage

1. Create a text file (e.g., `play.txt`) with the list of songs you want to play, each on a new line.
2. Run the `main.py` script, providing the file path as a command-line argument:

```
python main.py play.txt
```

The script will read each line from the file, search for the corresponding song on YouTube, download the audio, and play it using VLC.

## Code Overview

The script is divided into two main functions:

### `urlfind(text)`

This function takes a search query as input and uses the YTMusicAPI to search for songs on YouTube. It returns the URL of the first result.

### `mus(text)`

This function takes a search query, finds the YouTube URL for the first result, extracts the audio URL using pytube, and then uses VLC to play the audio. It runs in a loop until the audio playback is complete.

The main part of the script reads each line from the specified file (`play.txt`) and calls the `mus` function for each song in the file.

## Example

Assuming you have a file named `play.txt` with the following content:

```
When I Look Into Your Eyes
Heaven (Bryan Adams)
Nggyu
```

Running the script:

```
python main.py play.txt
```

will search for each song on YouTube, download the audio, and play it using VLC.
## Contributions Welcome!
Anyone is welcomed to take a look at this project, fix bugs, and contribute improvements. As a college student, I may not have the time to address all the issues promptly. Feel free to fork the repository, make changes, and submit pull requests. Your contributions will be highly appreciated, and together we can make this script even better.